﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_Autos
{
    public partial class Form1 : Form
    {
        ACCESO acceso = new ACCESO();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarColores();
        }

        void EnlazarColores()
        {
            comboBox2.DataSource = null;
            comboBox2.DataSource = acceso.ListarColores();
        }

        void EnlazarAutos()
        {
            dataGridView3.DataSource = null;
            dataGridView3.DataSource = acceso.ListarAutos();

        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            EnlazarColores();
            EnlazarAutos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sql = "insert into color (color) values ('" + textBox1.Text + "')";

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrio un error.");
            }

            EnlazarColores();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            COLOR color = (COLOR)comboBox2.SelectedItem;

            String sql = "Insert into auto (patente, marca, id_color) values ";

            sql += "('" + textBox2.Text + "','" + textBox3.Text + "'," + color.ID.ToString() + ")";
        
            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrio un error");
            }

            EnlazarAutos();
        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            COLOR color = (COLOR)comboBox2.SelectedItem;
            String sql;

            sql = "Update auto set patente ='" + textBox2.Text;
            sql += "', marca='" + textBox3.Text + "', id_color= " + color.ID.ToString() + " where patente = '"+ lblPatente.Text + "'";

            ACCESO acceso = new ACCESO();

            if (acceso.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Ok");
            }
            EnlazarAutos();
        }

        private void dataGridView3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            AUTO auto = (AUTO)dataGridView3.Rows[e.RowIndex].DataBoundItem;

            textBox2.Text = auto.Patente;

            textBox3.Text = auto.Marca;

            lblPatente.Text = auto.Patente;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ACCESO acceso = new ACCESO();

            foreach (DataGridViewRow registro in dataGridView3.SelectedRows)
            {
                AUTO auto = (AUTO)registro.DataBoundItem;

                string sql = "Delete from auto where patente='" + auto.Patente.ToString()+"'";

                acceso.Escribir(sql);
            }
            EnlazarAutos();
            acceso = null;
            GC.Collect();
        }
    }
}
