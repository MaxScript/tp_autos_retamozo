﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Autos
{
    class AUTO
    {
        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        private COLOR color;

        public COLOR Color
        {
            get { return color; }
            set { color = value; }
        }
    }
}
