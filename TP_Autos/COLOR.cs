﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP_Autos
{
    class COLOR
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string color;

        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        public override string ToString()
        {
            return color;
        }

    }
}
