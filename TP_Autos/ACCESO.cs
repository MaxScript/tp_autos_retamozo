﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace TP_Autos
{
    class ACCESO
    {
        private SqlConnection conexion;

        private void Abrir()
        {
            conexion = new SqlConnection();

            conexion.ConnectionString = @"Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=BaseDeDatos;Data Source=.\SQLEXPRESS";
            conexion.Open();
        }
        private void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }
        private SqlCommand CrearComando(string sql)
        {
            SqlCommand comando = new SqlCommand();

            comando.CommandText = sql;
            comando.Connection = conexion;
            comando.CommandType = CommandType.Text;

            return comando;
        }

        public List<COLOR> ListarColores()
        {
            List<COLOR> colores = new List<COLOR>();

            Abrir();

            string sql = "Select * from color";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                COLOR color = new COLOR();
                color.ID = int.Parse(lector["id_color"].ToString());
                color.Color = lector["Color"].ToString();

                colores.Add(color);
            }

            lector.Close();

            Cerrar();

            return colores;
        }

        public List<AUTO> ListarAutos()
        {
            List<AUTO> autos = new List<AUTO>();

            List<COLOR> colores = ListarColores();

            Abrir();

            string sql = "select * from auto";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                AUTO auto = new AUTO();
                auto.Patente = lector["PATENTE"].ToString();
                auto.Marca = lector["MARCA"].ToString();
                int indice = 0;
                while (auto.Color == null)
                {
                    
                    if (lector.GetInt32(2) == colores[indice].ID)
                    {
                        auto.Color = colores[indice];
                    }
                    else
                    {
                        indice++;
                    }
                }

                autos.Add(auto);
            }

            Cerrar();

            return autos;
        }


        public int Escribir (string sql)
        {
            int filasAfectadas = 0;
            Abrir();

            SqlCommand comando = CrearComando(sql);

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch(SqlException ex)
            {
                filasAfectadas = -1;
            }

            Cerrar();

            return filasAfectadas;
        }
    }
}
